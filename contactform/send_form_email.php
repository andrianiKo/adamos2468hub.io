<?php
if(isset($_POST['email']) && isset($_POST['sendTo'])) {
    $email_to = "";
    // EDIT THE 2 LINES BELOW AS REQUIRED
    if (strcmp($_POST['sendTo'] , "coi") == 0){
        $email_to = "coinformatics@gmail.com"; 
    } 
    if (strcmp($_POST['sendTo'] , "pavlos") == 0){
        $email_to = "ppavlikas@gmail.com"; 
    } 
    if (strcmp($_POST['sendTo'] , "panos") == 0){
        $email_to = "paneracl@gmail.com"; 
    } 
    if (strcmp($_POST['sendTo'] , "akis") == 0){
        $email_to = "asykopetritis@gmail.com"; 
    } 
    if (strcmp($_POST['sendTo'] , "andreas") == 0){
        $email_to = "ademost@yahoo.com"; 
    }
    if (strcmp($_POST['sendTo'] , "andonis") == 0){
        $email_to = "anthger@gmail.com"; 
    }            
    $email_subject = "Contact Form from COI";
 
    function died($error) {
        // your error code can go here
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['sendTo']) ||
        !isset($_POST['subject']) ||
        !isset($_POST['email']) ||
        !isset($_POST['comments'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
 
     
 
    $name = $_POST['name']; // required
    $subject = $_POST['subject']; // required
    $email_from = $_POST['email']; // required
    $comments = $_POST['comments']; // required
 
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$name)) {
    $error_message .= 'Το όνομα που δώσατε δεν φαίνεται να είναι έγκυρο.<br />';
  }
 
  if(strlen($comments) < 2) {
    $error_message .= 'Το κείμενο που προσπαθείτε να στείλετε δεν φαίνεται να είναι έγκυρο.<br />';
  }
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
     
 
    $email_message .= "Name: ".clean_string($name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
    $email_message .= "Subject: ".clean_string($subject)."\n";
    $email_message .= "Comments: ".clean_string($comments)."\n";
 
// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
?>
 
<!-- include your own success html here -->
 
Σας ευχαριστούμε για το μήνυμα! Θα επικοινωνήσουμε μαζί σας πολύ σύντομα.
 
<?php
 
}
?>
